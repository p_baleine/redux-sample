interface Window {
  fetch(url: string, init?: any): any;
}

declare var fetch: typeof window.fetch;
