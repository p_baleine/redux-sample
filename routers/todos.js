var express = require('express');
var router = express.Router();

var items = [{
  id: 0,
  text: "Hello, world!!",
  completed: false
}];

router.get('/', function(req, res) {
  res.send(items);
});

router.post('/', function(req, res) {
  var item = req.body;
  var id = items.push(item) - 1;
  item.id = id;
  res.send(item);
});

router.put('/:id', function(req, res) {
  var id = req.params.id;
  var i = items.filter(x => x.id == id)[0].id;
  var body = req.body;
  var item = items[i];
  if (!item) {
    return res.sendStatus(404);
  }
  item.title = body.title;
  item.completed = body.completed;
  res.sendStatus(200);
});

module.exports = router;
