import { createAction, Action } from 'redux-actions';
import { v1 } from 'node-uuid';

import { Todo } from '../models/todos';
import * as types from '../constants/ActionTypes';

const addTodo = createAction<Todo>(
  types.ADD_TODO,
  (text: string) => ({ key: v1(), text, completed: false })
);

const completeTodo = createAction<Todo>(
  types.COMPLETE_TODO,
  (todo: Todo) => todo
);

const setVisibilityFilter = createAction<string>(
  types.SET_VISIBILITY_FILTER,
  (filter: string) => filter
);

const receiveTodos = createAction<Todo[]>(
  types.RECEIVE_TODOS,
  (todos: Todo[]) => todos
);

function fetchTodos() {
  return  (dispatch, getState) => {
    return fetch('/todos')
    .then(response => response.json())
    .then(json => dispatch(receiveTodos(json)));
  };
}

function createTodo(text) {
  return (dispatch) => {
    dispatch(addTodo(text));
    return fetch('/todos', {
      method: 'POST',
      body: JSON.stringify({ text: text, completed: false }),
      headers: { 'content-type': 'application/json' }
    });
  }
}

function updateTodo(todo: Todo) {
  return (dispatch) => {
    dispatch(completeTodo(todo));
    return fetch('/todos/' + todo.id, {
      method: 'PUT',
      body: JSON.stringify(Object.assign({}, todo, { completed: !todo.completed })),
      headers: { 'content-type': 'application/json' }
    });
  }
}

export {
  createTodo,
  updateTodo,
  setVisibilityFilter,
  fetchTodos
};
