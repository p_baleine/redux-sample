import { combineReducers } from 'redux';
import { handleActions, Action } from 'redux-actions';
import { v1 } from 'node-uuid';

import { SHOW_ALL } from '../constants/VisibilityFilters';
import { Todo } from '../models/todos';
import {
  ADD_TODO, COMPLETE_TODO, SET_VISIBILITY_FILTER, RECEIVE_TODOS
} from '../constants/ActionTypes';

const todos = handleActions<Todo[]>({
  [ADD_TODO]: (state: Todo[], action: Action): Todo[] => {
    return [...state, action.payload];
  },

  [COMPLETE_TODO]: (state: Todo[], action: Action): Todo[] => {
    return state.map(todo =>
      todo.id === action.payload.id ?
        Object.assign({}, todo, { completed: !todo.completed }) :
        todo
    );
  },

  [RECEIVE_TODOS]: (state: Todo[], action: Action): Todo[] => {
    return action.payload.map(todo => Object.assign({}, todo, { key: v1() }));
  }
}, []);

const visibilityFilter = handleActions<string>({
  [SET_VISIBILITY_FILTER]: (state: string, action: Action): string => {
    return action.payload;
  }
}, SHOW_ALL);

export default combineReducers({
  visibilityFilter,
  todos
});
