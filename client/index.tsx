import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import App from './containers/App';
import configureStore from './configureStore';

const style = require('./style.css');
let store = configureStore({});
let rootElemt = document.getElementById('root');
rootElemt.id = style.root;

render(
  <Provider store={store}>
    <App />
  </Provider>,
  rootElemt
);
