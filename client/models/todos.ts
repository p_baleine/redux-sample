export type Todo = {
  id?: number,
  key: string,
  text: string,
  completed: boolean
};
