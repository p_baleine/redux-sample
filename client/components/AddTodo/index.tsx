import * as React from 'react';
const { Component } = React;

import { Todo } from '../../models/todos';

export interface IAddTodoProps {
  onAddClick: (Todo) => any
}

class AddTodo extends Component<IAddTodoProps, {}> {
  refs: {
    [string: string]: any;
    input: any;
  }

  render() {
    return (
      <form onSubmit={e => e.preventDefault()}>
        <input type='text' ref='input' />
        <button onClick={e => this.handleClick(e)}>
          Add
        </button>
      </form>
    );
  }

  handleClick(e) {
    const node = this.refs.input;
    const text = node.value.trim();
    this.props.onAddClick(text);
    node.value = '';
  }
}

export default AddTodo;
