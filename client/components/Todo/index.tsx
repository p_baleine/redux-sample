import * as React from 'react';
const { Component } = React;

const style = require('./style.css');

export interface ITodoProps {
  onClick: () => any
  completed: boolean,
  text: string,
  key: any
}

class Todo extends Component<ITodoProps, {}> {
  render() {
    return (
      <li className={`${style.item} ${this.props.completed ? style.completed : ''}`}
        onClick={this.props.onClick}>
        {this.props.text}
      </li>
    );
  }
}

export default Todo;
