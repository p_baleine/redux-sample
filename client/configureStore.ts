import { createStore, applyMiddleware } from 'redux';
import * as thunk from 'redux-thunk';

import todoApp from './reducers/todos';

const createStoreWithMiddleware = applyMiddleware(
  <any>thunk
)(createStore);

function configureStore(initialState) {
  return createStoreWithMiddleware(todoApp, initialState);
}

export default configureStore;
