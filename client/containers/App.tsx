import * as React from 'react';
import { connect } from 'react-redux';
const {Component} = React;

import {
  createTodo, updateTodo,
  setVisibilityFilter, fetchTodos
} from '../actions/todos';
import { SHOW_ALL, SHOW_COMPLETED, SHOW_ACTIVE } from '../constants/VisibilityFilters';
import { Todo } from '../models/todos';
import AddTodo from '../components/AddTodo';
import TodoList from '../components/TodoList';
import Footer from '../components/Footer';

const style = require('./style.css');

class App extends Component<any, any> {
  componentDidMount() {
      const { dispatch } = this.props;
      dispatch(fetchTodos());
  }

  render() {
    const { dispatch, visibleTodos, visibilityFilter } = this.props;
    return (
      <div>
        <h1>Todo</h1>
        <AddTodo
          onAddClick={text =>
            dispatch(createTodo(text))
          } />
        <TodoList
          todos={visibleTodos}
          onTodoClick={todo =>
            dispatch(updateTodo(todo))
          } />
        <Footer
          filter={visibilityFilter}
          onFilterChange={nextFilter =>
            dispatch(setVisibilityFilter(nextFilter))
          } />
        </div>
    );
  }
}

function selectTodos(todos: Todo[], filter: string) {
  switch (filter) {
    case SHOW_ALL:
      return todos;
    case SHOW_COMPLETED:
      return todos.filter(todo => todo.completed);
    case SHOW_ACTIVE:
      return todos.filter(todo => !todo.completed);
  }
}

function select(state) {
  return {
    visibleTodos: selectTodos(state.todos, state.visibilityFilter),
    visibilityFilter: state.visibilityFilter
  };
}

export default connect(select)(App);
